<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('galleries','GalleriesController');
Route::resource('orders','OrdersController');
Route::post('orders/measure', array('as'=>'measure','uses'=>'OrdersController@measure'));

Route::resource('measures','ItemsController');
Route::post('measures/itemdetail', array('as'=>'itemdetail','uses'=>'ItemsController@itemdetail'));

Route::get('pdf/quotationPDF/{id}', 'PdfController@quotationPDF');
Route::get('pdf/invoicePDF/{id}', 'PdfController@invoicePDF');