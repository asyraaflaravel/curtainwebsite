@extends('layouts.app')

@section('content')
    <a href="/orders/{{$order->id}}" class="btn btn-default">Back</a>
    <h1>Item Details</h1>
    <br><br>
    
    <div>
        Phone No :{{$order->name}}
    </div>
    <div>
        Phone No :{{$order->phone}}
    </div>
    <div>

        Email    :{{$order->email}}
    </div>
    <div>
        Address  :{{$order->address}}
    </div>
    <hr>
    <small>Ordered on {{$order->created_at}}</small>
    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
            <th>Width</th>
            <th>Length</th>
            <th>Notes</th>
            </tr>
        </thead>
        <tbody >
        @foreach($measures as $measure)
            <tr>
                <td class="col-md-3">{{$measure->width}}</td>
                <td class="col-md-3">{{$measure->length}}</td>
                <td class="col-md-7">{{$measure->comment}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
    @if(count($itemdetails) > 0)
    <table class="table table-bordered">
        <thead>
            <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            </tr>
        </thead>
        <tbody >
        @foreach($itemdetails as $itemdetail)
            <tr>
                <td class="col-md-6">{{$itemdetail->item}}</td>
                <td class="col-md-1">{{$itemdetail->quantity}}</td>
                <td class="col-md-2">{{$itemdetail->unitprice}}</td>
                <td class="col-md-2">{{$itemdetail->totalprice}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif
    <hr>
    {!!Form::open(array('action' => array('ItemsController@itemdetail')))!!}
        <table id="add-itemtable" class="table table-bordered">
            <thead>
                <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody >
            <tr>
                <td class="col-md-6"><input type="text" name="item[]" class="form-control" /></td>
                <td class="col-md-1"><input type="text" name="quantity[]" class="form-control" /></td>
                <td class="col-md-3"><input type="text" name="unitprice[]" class="form-control" /></td>
                <td class="col-md-1"><button type="button" class="btn">Delete</button></td>
            </tr>
            </tbody>
        </table>
        <div class="action-buttons">
            <button id="add-item" type="button" class="btn btn-default">New Item</button>
            {{Form::hidden('customerid', $order->id)}}
            {{Form::submit('Save', array('class'=>'btn btn-success'))}}
        </div>
    {!!Form::close()!!}
    @if(count($measures) > 0)
        <div class="action-buttons pull-right">
            <a href="/pdf/invoicePDF/{{$order->id}}" class="btn btn-default" target="_blank">Print Invoice</a>
        </div>
    @endif
@endsection