@extends('layouts.app')

@section('content')
    <a href="/orders" class="btn btn-default">Back</a>
    <h1>Item Measurement</h1>
    <br><br>
    
    <div>
        Customer Name :{{$order->name}}
    </div>
    <div>
        Phone No :{{$order->phone}}
    </div>
    <div>

        Email    :{{$order->email}}
    </div>
    <div>
        Address  :{{$order->address}}
    </div>
    <hr>
    <small>Ordered on {{$order->created_at}}</small>
    <hr>
    @if(!Auth::guest())
        @if($order->status == "NEW")
            {!!Form::open(['action' => ['OrdersController@update', $order->id], 'method' => 'POST'])!!}
                {{Form::hidden('_method', 'PUT')}}
                {{Form::hidden('status', 'APPROVED')}}
                {{Form::submit('Approve', ['class' => 'btn btn-success' , 'value' => 'APPROVE'])}}
            {!!Form::close()!!}
        @endif
        @if($order->status == "APPROVED")

            @if(count($measures) > 0)
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th>Width</th>
                    <th>Length</th>
                    <th>Notes</th>
                    </tr>
                </thead>
                <tbody >
                @foreach($measures as $measure)
                    <tr>
                        <td class="col-md-3">{{$measure->width}}</td>
                        <td class="col-md-3">{{$measure->length}}</td>
                        <td class="col-md-7">{{$measure->comment}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
            <hr>

            {!!Form::open(array('action' => array('OrdersController@measure')))!!}
                <table id="add-me" class="table table-bordered">
                    <thead>
                        <tr>
                        <th>Width</th>
                        <th>Length</th>
                        <th>Notes</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody >
                    <tr>
                        <td class="col-md-3"><input type="text" name="width[]" class="form-control" /></td>
                        <td class="col-md-3"><input type="text" name="length[]" class="form-control" /></td>
                        <td class="col-md-7"><input type="text" name="comment[]" class="form-control" /></td>
                        <td class="col-md-2"><button type="button" class="btn">Delete</button></td>
                    </tr>
                    </tbody>
                </table>
                <div class="action-buttons">
                    <button id="add-form" type="button" class="btn btn-default">New Measurement</button>
                    {{Form::hidden('customerid', $order->id)}}
                    {{Form::submit('Save', array('class'=>'btn btn-success'))}}
                </div>
            {!!Form::close()!!}
            @if(count($measures) > 0)
                <div class="action-buttons pull-right">
                    <a href="/pdf/quotationPDF/{{$order->id}}" class="btn btn-default" target="_blank">Print Quotation</a>
                    <a href="/measures/{{$order->id}}" class="btn btn-success">Done Measure</a>
                </div>
            @endif
        @endif
    @endif
@endsection