@extends('layouts.app')

@section('content')
    <h1>New Order</h1>
    {!! Form::open(['action' => 'OrdersController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('address', 'Address')}}
            {{Form::textarea('address', '', ['class' => 'form-control', 'placeholder' => 'Address'])}}
        </div>
        <div class="form-group">
            {{form::label('phone', 'Phone No')}}
            {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Phone No'])}}
        </div>
        <div class="form-group">
            {{form::label('email', 'Email')}}
            {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
        </div>

        <div class="form-group">
            <div class="col-md-6">
                {!!Recaptcha::render()!!}
                {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
            </div>
            
        </div>
        
    {!! Form::close() !!}
@endsection