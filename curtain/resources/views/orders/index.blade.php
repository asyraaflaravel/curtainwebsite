@extends('layouts.app')

@section('content')
@if (Auth::guest())
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="/orders/create" class="btn btn-primary">Request Order</a>
                </div>
            </div>
        </div>
    </div>
</div>
@else
@if(count($orders) > 0)
        @foreach($orders as $order)
            <div class="well">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/orders/{{$order->id}}">{{$order->name}}</a></h3>
                        <hr>
                        <div>
                            {{$order->address}}
                        </div> 
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <h3>{{$order->status}}</h3>
                    </div>
                </div>
            </div>
        @endforeach
        {{$orders->links()}}
    @else
        <p>No Photo</p>
    @endif
@endif
@endsection