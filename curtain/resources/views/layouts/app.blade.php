<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Curtain') }}</title>
    <!-- Styles -->
    
</head>

<body>
    <div id="app">
        @include('inc.navbar')
        <div class="container">
            @include('inc.messages')
            @yield('content')
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="http://jueshamenterprise.000webhostapp.com/js/css/app.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
    <script>
        i = 0;
        $('#add-form').click(function() {
        i++;
        $('#add-me').append(
       '<tbody><tr id="'+i+'">'+
         '<td class="col-md-3">'
            +'<input type="text" name="width[]" class="form-control"/>'
        +'</td>'
        +'<td class="col-md-3">'
            +'<input type="text" name="length[]" class="form-control" />'
        +'</td>'
        +'<td class="col-md-7">'
            +'<input type="text" name="comment[]" class="form-control" />'
        +'</td>'
        +'<td class="col-md-2">'
            +'<button id="delete" type="button" class="btn">Delete</button>'
        +'</td>'
         +'</tr></tbody>'
        );
        });
    </script>
    <script>
        $(document).on('click', 'button.btn', function () {
         $(this).closest('tr').remove();
        });
    </script>
    <script>
        i = 0;
        $('#add-item').click(function() {
        i++;
        $('#add-itemtable').append(
       '<tbody><tr id="'+i+'">'+
         '<td class="col-md-6">'
            +'<input type="text" name="item[]" class="form-control"/>'
        +'</td>'
        +'<td class="col-md-1">'
            +'<input type="text" name="quantity[]" class="form-control" />'
        +'</td>'
        +'<td class="col-md-3">'
            +'<input type="text" name="unitprice[]" class="form-control" />'
        +'</td>'
        +'<td class="col-md-1">'
            +'<button id="delete" type="button" class="btn">Delete</button>'
        +'</td>'
         +'</tr></tbody>'
        );
        });
    </script>
</body>
<div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">© 2018 - Site Built By Mr. 
           <a href="https://www.facebook.com/asyraafaizzad94" target="_blank">Asyraaf</a>
      </p>
      <p class="navbar-text pull-right">
        <a href="https://www.facebook.com/sharifah.junaidah.73" target="_blank">
              <i class="fa fa-2x fa-fw fa-facebook"></i>
		</a>
		<a href="https://www.instagram.com/juesha73/" target="_blank">
                <i class="fa fa-2x fa-fw fa-instagram"></i></a>
        </p>
    </div>
</div>
    <!--/.footer-->
</html>