@extends('layouts.app')

@section('content')
    <a href="/galleries" class="btn btn-default">Back</a>
    <h1>{{$gallery->title}}</h1>
    <img style="width:100%" src="/storage/cover_images/{{$gallery->cover_image}}">
    <br><br>
    <div>
        {!!$gallery->body!!}
    </div>
    <hr>
    <small>Written on {{$gallery->created_at}}</small>
    <hr>
    @if(!Auth::guest())
        <a href="/galleries/{{$gallery->id}}/edit" class="btn btn-default">Edit</a>
        {!!Form::open(['action' => ['GalleriesController@destroy', $gallery->id], 'method' => 'POST', 'class' => 'pull-right'])!!}

            {{Form::hidden('_method', 'DELETE')}}

            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}

        {!!Form::close()!!}
    @endif
@endsection