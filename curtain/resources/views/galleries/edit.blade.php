@extends('layouts.app')

@section('content')
    <h1>Edit</h1>
    {!! Form::open(['action' => ['GalleriesController@update', $gallery->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{form::label('title', 'Title')}}
            {{Form::text('title', $gallery->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $gallery->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection