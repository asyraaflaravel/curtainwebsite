@extends('layouts.app')

@section('content')
    <h1>Gallery</h1>
    @if(count($galleries) > 0)
        @foreach($galleries as $gallery)
            <div class="well">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/storage/cover_images/{{$gallery->cover_image}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/galleries/{{$gallery->id}}">{{$gallery->title}}</a></h3>
                    </div>
                </div>
            </div>
        @endforeach
        {{$galleries->links()}}
    @else
        <p>No Photo</p>
    @endif
@endsection