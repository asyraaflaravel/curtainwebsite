@extends('layouts.app')

@section('content')
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
      
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @if(count($galleries) > 0)
                <div class="item active">
                    <img style="width:100%" src="/storage/cover_images/{{$galleries[0]->cover_image}}">
                    <div class="carousel-caption">
                        <h3><a>{{$galleries[0]->title}}</a></h3>
                    </div>
                </div>
                @if(count($galleries) > 1)
                    <div class="item">
                        <img style="width:100%" src="/storage/cover_images/{{$galleries[1]->cover_image}}">
                        <div class="carousel-caption">
                            <h3><a>{{$galleries[1]->title}}</a></h3>
                        </div>
                    </div>
                    @if(count($galleries) > 2)
                        <div class="item">
                            <img style="width:100%" src="/storage/cover_images/{{$galleries[2]->cover_image}}">
                            <div class="carousel-caption">
                                <h3><a>{{$galleries[2]->title}}</a></h3>
                            </div>
                        </div>
                    @else
                        <div class="item">
                            <img style="width:100%" src="/storage/cover_images/noimage.gif">
                            <div class="carousel-caption">
                            </div>
                        </div>
                    @endif
                @else
                    <div class="item">
                        <img style="width:100%" src="/storage/cover_images/noimage.gif">
                        <div class="carousel-caption">
                        </div>
                    </div>
                    <div class="item">
                        <img style="width:100%" src="/storage/cover_images/noimage.gif">
                        <div class="carousel-caption">
                        </div>
                    </div>
                @endif
            @else
                <div class="item active">
                    <img style="width:100%" src="/storage/cover_images/noimage.gif">
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <img style="width:100%" src="/storage/cover_images/noimage.gif">
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <img style="width:100%" src="/storage/cover_images/noimage.gif">
                    <div class="carousel-caption">
                    </div>
                </div>
            @endif
        </div>
      
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
@endsection
