<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title></title>
  </head>
  <body>
    <h1>Invoice</h1>
    <br><br>
    
    <div>
        Customer Name   :{{$order->name}}
    </div>
    <div>
        Phone No        :{{$order->phone}}
    </div>
    <div>

        Email           :{{$order->email}}
    </div>
    <div>
        Address         :{{$order->address}}
    </div>
    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            </tr>
        </thead>
        <tbody >
        @foreach($itemdetails as $itemdetail)
            <tr>
                <td width="55%">{{$itemdetail->item}}</td>
                <td width="15%">{{$itemdetail->quantity}}</td>
                <td width="15%">{{$itemdetail->unitprice}}</td>
                <td width="15%">{{$itemdetail->totalprice}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</html>