<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title></title>
  </head>
  <body>
    <h1>Quotation</h1>
    <br><br>
    
    <div>
        Customer Name   :{{$order->name}}
    </div>
    <div>
        Phone No        :{{$order->phone}}
    </div>
    <div>

        Email           :{{$order->email}}
    </div>
    <div>
        Address         :{{$order->address}}
    </div>
    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
            <th>Width</th>
            <th>Length</th>
            <th>Notes</th>
            </tr>
        </thead>
        <tbody >
        @foreach($measures as $measure)
            <tr>
                <td class="col-md-7">{{$measure->width}}</td>
                <td class="col-md-3">{{$measure->length}}</td>
                <td class="col-md-3">{{$measure->comment}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
  </body>
</html>