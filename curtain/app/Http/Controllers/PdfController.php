<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\item_measure;
use App\ItemDetail;
use PDF;

class PdfController extends Controller
{
    public function quotationPDF($id)
    {   
        $order = Order::find($id);
        $measures = item_measure::where('customer_id',$id)->get();
  
        $pdf = PDF::loadView('pdfs.quotationPDF', compact('measures','order'));
        
        return $pdf->download($order->name.' Quotation.pdf');
  
    }

    public function invoicePDF($id)
    {   
        $order = Order::find($id);
        $itemdetails = ItemDetail::where('customer_id',$id)->get();
  
        $pdf = PDF::loadView('pdfs.invoicePDF', compact('itemdetails','order'));
        
        return $pdf->download($order->name.' Invoice.pdf');
  
    }
}
