<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\item_measure;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('created_at','asc')->paginate(10);
        return view('orders.index')->with('orders',$orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);

        $orders = new Order;
        $orders->name = $request->input('name');
        $orders->address = $request->input('address');
        $orders->phone = $request->input('phone');
        $orders->email = $request->input('email');
        $orders->status = "NEW";
        $orders->save();

        return redirect('/orders')->with('success', 'Order Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        
        $measure = item_measure::where('customer_id',$id)->get();
        return view('orders.show')->with('order', $order)->with('measures', $measure);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = $request->input('status');
        $order->save();

        return redirect('/orders')->with('success', 'Approved');
    }

    public function measure(Request $request)
    {
        $id = $request->customerid;

        foreach($request->width as $key => $v)
        {
            $data = array('customer_id'=>$id,
                        'width' =>$v,
                        'length' =>$request->length[$key],
                        'comment' =>$request->comment[$key]);

            item_measure::insert($data);
        }

        return back()->with('success', 'Measurement uploaded');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
