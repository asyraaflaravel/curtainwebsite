<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\item_measure;
use App\ItemDetail;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $measures = item_measure::where('customer_id',$id)->get();
        $itemdetails = ItemDetail::where('customer_id',$id)->get();
        
        return view('calculations.calculate')->with('order', $order)->with('measures', $measures)->with('itemdetails', $itemdetails);
    }

    public function itemdetail(Request $request)
    {
        $id = $request->customerid;

        foreach($request->item as $key => $v)
        {
            $totalprice = $request->quantity[$key] * $request->unitprice[$key];
            
            $data = array('customer_id'=>$id,
                        'item' =>$v,
                        'quantity' =>$request->quantity[$key],
                        'unitprice' =>$request->unitprice[$key],
                        'totalprice' =>$totalprice);

            ItemDetail::insert($data);
        }

        return back()->with('success', 'Item Details uploaded');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
