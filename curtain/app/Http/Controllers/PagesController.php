<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class PagesController extends Controller
{
    public function index()
    {
        $galleries = Gallery::orderBy('created_at','desc')->take(3)->get();
        return view('pages.index')->with('galleries',$galleries);
    }
}
